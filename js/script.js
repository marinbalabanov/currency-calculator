// Funktion, die einen Währungsbetrag auf zwei Nachkommastellen korrekt kaufmännisch rundet und den Punkt auf ein Komma korrigiert
function umrechnungRunden(betrag, stellen) {
    console.log('Ungerundeter, aber umgerechneter Betrag lautet: ' + betrag)
    var gerundeteZahl = (Number(Math.round(betrag+'e'+stellen)+'e-'+stellen)).toString();
    var ausgabeZahlKomma = gerundeteZahl.replace(".",",")
    return ausgabeZahlKomma;
};

// Liste der Währungskürzel holen und in die Währungsselektionsliste einfügen
$.getJSON('http://wifi.1av.at/currency.php', function(result){

    $.each(result, function(titel, waehrungsliste){
        console.log(titel);
        for(var aktuell of waehrungsliste) {
            $( '<option>' ).html(aktuell).appendTo('#verfuegbareWaehrungen');
        };
    });

});

// 1. Holt den eingegebenen Eurobetrag und die ausgewählte Währung.
// 2. Prüft, ob der Betrag tatsächlich eine Zahl ist (wenn nicht, dann wird der Betrag auf 0 gesetzt).
// 3. Holt anhand der ausgewählten Währung den Pfad zum Bild der passenden Landesfahne von der API.
// 4. Schickt den Betrag und die Währung an die API, erhält dann den umgerechneten Betrag.
// 5. Fügt den umgerechneten, gekürzten und komma-korrigierten Betrag in das Ausgabefeld ein.
// 6. Fügt die Landesahne in die vorgesehene Spalte ein.
$('#umrechnenButton').click(function(formularevent){

    formularevent.preventDefault();
    var eingabeBetrag = parseInt($("#waehrungsEingabe").val());
    if(Number.isNaN(eingabeBetrag)) {
        eingabeBetrag = 0;
    };
    var ausgewaehlteWaehrung = $("#verfuegbareWaehrungen").val();

    $.ajax({
        url: 'http://wifiweb.bplaced.net/getflag.php',
        method: 'post',
        data: {
            currency: ausgewaehlteWaehrung
        },
        success: function( response ) {
            currencyFlag = response.flag;
        }
    });

    $.ajax({
        url: 'http://wifiweb.bplaced.net/currency_calc.php',
        method: 'post',
        data: {
            wieviel: eingabeBetrag,
            waehrung: ausgewaehlteWaehrung
        },
        success: function( response ) {
            $('#waehrungsAusgabe').val(ausgewaehlteWaehrung + ' ' + umrechnungRunden(response, 2));
            $('#landesFlagge').html('<img class="flagStyle" src="' + currencyFlag + '" alt="Landesfahne für ' + ausgewaehlteWaehrung + '">')
        }
    });

});
